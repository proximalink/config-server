FROM python:3.10.11-slim-bullseye
WORKDIR /app

# copy files
COPY main.py .
COPY requirements.txt .

RUN pip install -r requirements.txt

EXPOSE 8000
CMD ["uvicorn", "--host", "0.0.0.0", "main:app"]