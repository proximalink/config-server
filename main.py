from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
import yaml, json

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

with open("proxima-cfg.yaml", "r") as f:
    try:
        pve_config = yaml.safe_load(f)
        print(pve_config)
    except yaml.YAMLError as e:
        print(e)


@app.get("/config")
def config():
    return json.dumps(pve_config)
